package com.github.axet.torrentclient.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.axet.androidlibrary.widgets.AppCompatFullscreenThemeActivity;
import com.github.axet.torrentclient.R;
import com.github.axet.androidlibrary.sound.MediaPlayerCompat;
import com.github.axet.torrentclient.app.TorrentApplication;
import com.github.axet.torrentclient.app.TorrentPlayer;

public class PlayerActivity extends AppCompatFullscreenThemeActivity {
    public static String CLOSE = PlayerActivity.class.getCanonicalName() + ".CLOSE";
    public static String RESUME = PlayerActivity.class.getCanonicalName() + ".RESUME";

    public MediaPlayerCompat.PlayerView exoplayer;
    TorrentPlayer.Receiver playerReceiver;
    long playerTorrent;
    View close;
    View controls;
    View frame;
    TorrentPlayer player;
    int playingIndex;

    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            setFullscreen(true);
        }
    };

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PlayerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    public static void closeActivity(Context context) {
        Intent intent = new Intent(context, PlayerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setAction(CLOSE);
        context.startActivity(intent);
    }

    @Override
    public int getAppTheme() {
        return R.style.FullscreenTheme;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainActivity.showLocked(getWindow());

        setContentView(R.layout.activity_player);

        String a = getIntent().getAction();
        if (a != null) {
            if (a.equals(CLOSE)) {
                player = null;
                finish();
                return;
            }
        }

        player = TorrentApplication.from(this).player;

        if (player == null) { // player closed before activity started
            finish();
            return;
        }

        playingIndex = player.getPlaying();

        frame = findViewById(R.id.player_frame);
        exoplayer = (MediaPlayerCompat.PlayerView) findViewById(R.id.fullscreen_content);
        close = findViewById(R.id.player_close);
        controls = findViewById(R.id.player_controls);

        final TextView playerPos = (TextView) findViewById(R.id.player_pos);
        final TextView playerDur = (TextView) findViewById(R.id.player_dur);
        final ImageView fab_prev = (ImageView) findViewById(R.id.player_prev);
        final ImageView fab_next = (ImageView) findViewById(R.id.player_next);
        final ImageView fab_play = (ImageView) findViewById(R.id.player_play);
        final SeekBar seek = (SeekBar) findViewById(R.id.player_seek);

        frame.setBackgroundColor(Color.BLACK);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fab_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TorrentPlayer p = player;
                int i = p.getPlaying();
                i = i - 1;
                if (i < 0)
                    i = p.getSize() - 1;
                p.play(i);
                p.notifyNext();
            }
        });

        fab_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TorrentPlayer p = player;
                int i = p.getPlaying();
                i = i + 1;
                if (i >= p.getSize())
                    i = 0;
                p.play(i);
                p.notifyNext();
            }
        });

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    player.seek(progress);
                    playerPos.setText(TorrentApplication.formatDuration(PlayerActivity.this, progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        playerReceiver = new TorrentPlayer.Receiver(this) {
            @Override
            public void onReceive(Context context, Intent intent) {
                String a = intent.getAction();
                if (a.equals(TorrentPlayer.PLAYER_PROGRESS)) {
                    playerTorrent = intent.getLongExtra("t", -1);
                    long pos = intent.getLongExtra("pos", 0);
                    long dur = intent.getLongExtra("dur", 0);
                    boolean play = intent.getBooleanExtra("play", false);
                    playerPos.setText(TorrentApplication.formatDuration(context, pos));
                    playerDur.setText(TorrentApplication.formatDuration(context, dur));
                    if (play)
                        fab_play.setImageResource(R.drawable.ic_pause_24dp);
                    else
                        fab_play.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                    seek.setMax((int) dur);
                    seek.setProgress((int) pos);
                }
                if (a.equals(TorrentPlayer.PLAYER_NEXT)) {
                    playerTorrent = intent.getLongExtra("t", -1);
                    playerPos.setText(TorrentApplication.formatDuration(context, 0));
                    playerDur.setText(TorrentApplication.formatDuration(context, 0));
                    seek.setMax(0);
                    seek.setProgress(0);
                    fab_play.setImageResource(R.drawable.ic_pause_24dp);
                }
                if (a.equals(TorrentPlayer.PLAYER_STOP)) {
                    finish();
                }
            }
        };

        player.play(exoplayer);

        fab_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player.isPlayingSound()) {
                    player.pause();
                } else {
                    player.resume();
                }
            }
        });

        frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        close();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(100);
    }

    @Override
    public void hideSystemUI() {
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LOW_PROFILE);
        controls.setVisibility(View.GONE);
    }

    @Override
    public void showSystemUI() {
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        controls.setVisibility(View.VISIBLE);
    }

    public void delayedHide(int delayMillis) {
        handler.removeCallbacks(mHideRunnable);
        handler.postDelayed(mHideRunnable, delayMillis);
    }

    @Override
    public void finish() {
        super.finish();
        MainActivity.startActivity(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String a = intent.getAction();
        if (a != null) {
            if (a.equals(CLOSE)) {
                player = null;
                finish();
                return;
            }
        }

        if (player != null)
            player.play(exoplayer);
        delayedHide(100);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (player != null)
            player.hide(exoplayer);
    }

    void close() {
        if (player != null) {
            player.close(exoplayer);
            player = null;
        }
        if (playerReceiver != null) {
            playerReceiver.close();
            playerReceiver = null;
        }
    }
}
