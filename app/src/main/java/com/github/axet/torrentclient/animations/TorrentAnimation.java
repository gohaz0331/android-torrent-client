package com.github.axet.torrentclient.animations;

import android.view.View;
import android.view.animation.Animation;

import com.github.axet.androidlibrary.animations.ExpandAnimation;
import com.github.axet.androidlibrary.animations.MarginAnimation;
import com.github.axet.androidlibrary.widgets.HeaderRecyclerView;
import com.github.axet.torrentclient.R;

public class TorrentAnimation extends ExpandAnimation {

    public static Animation apply(final HeaderRecyclerView list, final View v, final boolean expand, boolean animate) {
        return apply(new LateCreator() {
            @Override
            public MarginAnimation create() {
                TorrentAnimation a = new TorrentAnimation(list, v, expand);
                if (expand)
                    atomicExpander = a;
                return a;
            }
        }, v, expand, animate);
    }

    public TorrentAnimation(HeaderRecyclerView list, View v, boolean expand) {
        super(list, v, v.findViewById(R.id.recording_player), v.findViewById(R.id.torrent_expand), expand);
    }

}
