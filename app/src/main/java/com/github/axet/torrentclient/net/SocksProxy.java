package com.github.axet.torrentclient.net;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.conn.socket.ConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.socket.LayeredConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.socket.PlainConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.protocol.HttpContext;

public class SocksProxy implements Proxy {

    public static final String SOCKS = "socks";

    String host;
    String port;

    public class SocketFactory implements ConnectionSocketFactory {
        ConnectionSocketFactory base;

        public SocketFactory(ConnectionSocketFactory b) {
            base = b;
        }

        @Override
        public Socket createSocket(HttpContext context) throws IOException {
            return new Socket(createProxy());
        }

        @Override
        public Socket connectSocket(int connectTimeout, Socket sock, HttpHost host, InetSocketAddress remoteAddress, InetSocketAddress localAddress, HttpContext context) throws IOException {
            return base.connectSocket(connectTimeout, sock, host, remoteAddress, localAddress, context);
        }
    }

    public class SSLSocketFactory implements LayeredConnectionSocketFactory {
        LayeredConnectionSocketFactory base;

        public SSLSocketFactory(ConnectionSocketFactory b) {
            base = (LayeredConnectionSocketFactory) b;
        }

        @Override
        public Socket createSocket(HttpContext context) throws IOException {
            return new Socket(createProxy());
        }

        @Override
        public Socket connectSocket(int connectTimeout, Socket sock, HttpHost host, InetSocketAddress remoteAddress, InetSocketAddress localAddress, HttpContext context) throws IOException {
            return base.connectSocket(connectTimeout, sock, host, remoteAddress, localAddress, context);
        }

        @Override
        public Socket createLayeredSocket(Socket socket, String target, int port, HttpContext context) throws IOException, UnknownHostException {
            return base.createLayeredSocket(socket, target, port, context);
        }
    }

    public SocksProxy(final HttpProxyClient c) {
        c.http.base = new SocketFactory(PlainConnectionSocketFactory.getSocketFactory());
        c.https.base = new SSLSocketFactory(SSLConnectionSocketFactory.getSocketFactory());
    }

    public SocksProxy(final HttpProxyClient c, String host, String port) {
        this(c);
        this.host = host;
        this.port = port;
    }

    java.net.Proxy createProxy() {
        return new java.net.Proxy(java.net.Proxy.Type.SOCKS, new InetSocketAddress(host, Integer.valueOf(port))); // network on main thread (host resolve)
    }

    @Override
    public void close() {
    }

    @Override
    public void filter(HttpRequest request, HttpContext context) {
    }
}
