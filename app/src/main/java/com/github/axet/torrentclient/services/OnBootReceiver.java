package com.github.axet.torrentclient.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.torrentclient.activities.BootActivity;
import com.github.axet.torrentclient.app.TorrentApplication;

public class OnBootReceiver extends BroadcastReceiver {
    String TAG = OnBootReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent i) {
        Log.d(TAG, "onReceive");
        OptimizationPreferenceCompat.setBootInstallTime(context, TorrentApplication.PREFERENCE_BOOT, System.currentTimeMillis());
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        if (shared.getBoolean(TorrentApplication.PREFERENCE_START, false))
            BootActivity.createApplication(context);
    }
}
