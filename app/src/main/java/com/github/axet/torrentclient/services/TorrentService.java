package com.github.axet.torrentclient.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.services.PersistentService;
import com.github.axet.androidlibrary.widgets.CacheImagesAdapter;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.ProximityShader;
import com.github.axet.androidlibrary.widgets.RemoteNotificationCompat;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.BootActivity;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.app.Storage;
import com.github.axet.torrentclient.app.TorrentApplication;
import com.github.axet.torrentclient.app.TorrentPlayer;

public class TorrentService extends PersistentService {
    public static final String TAG = TorrentService.class.getSimpleName();

    public static final int NOTIFICATION_TORRENT_ICON = 1;
    public static final int NOTIFICATION_DOWNLOAD_ICON = 10; // 10 + number of torrents

    public static String TITLE = "TITLE";
    public static String PLAYER = "player";
    public static String PLAYING = "playing";

    public static String UPDATE_NOTIFY = TorrentService.class.getCanonicalName() + ".UPDATE_NOTIFY";
    public static String SHOW_ACTIVITY = TorrentService.class.getCanonicalName() + ".SHOW_ACTIVITY";
    public static String PAUSE_BUTTON = TorrentService.class.getCanonicalName() + ".PAUSE_BUTTON";

    static {
        OptimizationPreferenceCompat.REFRESH = 5 * AlarmManager.MIN1;
    }

    {
        id = NOTIFICATION_TORRENT_ICON;
    }

    TorrentReceiver receiver;
    MediaSessionCompat msc;
    PendingIntent pause;

    public static void startService(Context context, String title) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.putBoolean(TorrentApplication.PREFERENCE_RUN, true);
        edit.commit();
        Intent i = new Intent(context, TorrentService.class).setAction(UPDATE_NOTIFY).putExtra(TITLE, title);
        OptimizationPreferenceCompat.startService(context, i);
    }

    public static Intent createIntent(String title, String player, boolean playing) {
        return new Intent(UPDATE_NOTIFY)
                .putExtra(TITLE, title)
                .putExtra(PLAYER, player)
                .putExtra(PLAYING, playing);
    }

    public static void updateNotify(Context context, String title, String player, boolean playing) {
        context.sendBroadcast(createIntent(title, player, playing));
    }

    public static void stopService(Context context) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.putBoolean(TorrentApplication.PREFERENCE_RUN, false);
        edit.commit();
        context.stopService(new Intent(context, TorrentService.class));
    }

    @SuppressLint("RestrictedApi")
    public static void notifyDone(Context context, Storage.Torrent t, int i) {
        RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Builder(context, R.layout.notifictaion);

        PendingIntent main = PendingIntent.getService(context, 0,
                new Intent(context, TorrentService.class).setAction(TorrentService.SHOW_ACTIVITY),
                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setViewVisibility(R.id.notification_player, View.GONE);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        builder.setTheme(TorrentApplication.getTheme(context, R.style.AppThemeLight, R.style.AppThemeDark))
                .setChannel(TorrentApplication.from(context).channelDownloads)
                .setImageViewTint(R.id.icon_circle, builder.getThemeColor(R.attr.colorButtonNormal))
                .setTitle(context.getString(R.string.app_name))
                .setText(t.name())
                .setMainIntent(main)
                .setAdaptiveIcon(R.mipmap.ic_launcher_foreground)
                .setSmallIcon(R.drawable.ic_downloaded)
                .setAutoCancel(true)
                .setLights(Color.BLUE, 500, 500)
                .setSound(alarmSound);

        NotificationManagerCompat nm = NotificationManagerCompat.from(context);
        nm.notify(NOTIFICATION_DOWNLOAD_ICON + i, builder.build());
    }

    public class TorrentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getAction();
            if (a == null)
                return;
            if (a.equals(UPDATE_NOTIFY)) {
                updateIcon(intent);
                return;
            }
            Log.d(TAG, "TorrentReceiver " + intent); // skip freq update log messages
            MediaButtonReceiver.handleIntent(msc, intent);
        }
    }

    public TorrentService() {
    }

    boolean isRunning() {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        return shared.getBoolean(TorrentApplication.PREFERENCE_RUN, false);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        receiver = new TorrentReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(UPDATE_NOTIFY);
        registerReceiver(receiver, filter);

        if (!isRunning()) {
            stopSelf();
            return;
        }
    }

    @Override
    public void onCreateOptimization() {
        optimization = new PersistentService.ServiceReceiver(TorrentApplication.PREFERENCE_OPTIMIZATION, TorrentApplication.PREFERENCE_NEXT) {
            @Override
            public void check() {
                ping();
            }

            @Override
            public void updateIcon() { // never received by Torrent Client, ignore
            }
        };
        optimization.create();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand " + intent);

        if (!isRunning()) {
            stopSelf();
            return START_NOT_STICKY;
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onRestartCommand() {
        BootActivity.createApplication(this);
    }

    @Override
    public void onStartCommand(Intent intent) {
        String a = intent.getAction();
        if (a != null) {
            if (a.equals(UPDATE_NOTIFY)) {
                updateIcon(intent);
            }
            if (a.equals(SHOW_ACTIVITY)) {
                ProximityShader.closeSystemDialogs(this);
                MainActivity.startActivity(this);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        sendBroadcast(new Intent(TorrentApplication.SAVE_STATE));

        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }

        headset(false, false);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public Notification build(Intent intent) {
        String title = intent.getStringExtra(TITLE);
        String player = intent.getStringExtra(PLAYER);
        boolean playing = intent.getBooleanExtra(PLAYING, false);

        PendingIntent main = PendingIntent.getService(this, 0,
                new Intent(this, TorrentService.class).setAction(SHOW_ACTIVITY),
                PendingIntent.FLAG_UPDATE_CURRENT);

        // PendingIntent pe = PendingIntent.getService(this, 0,
        //        new Intent(this, TorrentService.class).setAction(PAUSE_BUTTON),
        //        PendingIntent.FLAG_UPDATE_CURRENT);

        pause = PendingIntent.getBroadcast(this, 0,
                new Intent(TorrentPlayer.PLAYER_PAUSE),
                PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Builder(this, R.layout.notifictaion);

        // boolean pause = false; // getStorage().isPause();
        // view.setOnClickPendingIntent(R.id.notification_pause, pe);
        // view.setImageViewResource(R.id.notification_pause, pause ? R.drawable.play : R.drawable.pause);

        if (player == null || player.isEmpty()) {
            builder.setViewVisibility(R.id.notification_play, View.GONE);
            builder.setViewVisibility(R.id.notification_playing, View.GONE);
            headset(false, playing);
        } else {
            builder.setViewVisibility(R.id.notification_play, View.VISIBLE);
            builder.setViewVisibility(R.id.notification_playing, View.VISIBLE);
            builder.setTextViewText(R.id.notification_play, player);
            builder.setImageViewResource(R.id.notification_playing, playing ? R.drawable.ic_pause_24dp : R.drawable.ic_play_arrow_black_24dp);
            headset(true, playing);
        }
        builder.setOnClickPendingIntent(R.id.notification_playing, pause);

        builder.setTheme(TorrentApplication.getTheme(this, R.style.AppThemeLight, R.style.AppThemeDark))
                .setChannel(TorrentApplication.from(this).channelStatus)
                .setMainIntent(main)
                .setTitle(getString(R.string.app_name))
                .setText(title)
                .setImageViewTint(R.id.icon_circle, builder.getThemeColor(R.attr.colorButtonNormal))
                .setWhen(notification)
                .setAdaptiveIcon(R.mipmap.ic_launcher_foreground)
                .setSmallIcon(R.drawable.ic_launcher_notification)
                .setOngoing(true)
                .setOnlyAlertOnce(true);

        TorrentApplication app = TorrentApplication.from(this);
        if (app.player != null) {
            Bitmap bm = app.player.getArtwork();
            if (bm != null) {
                bm = CacheImagesAdapter.createThumbnail(bm);
                builder.setViewVisibility(R.id.artwork, View.VISIBLE);
                builder.setImageViewBitmap(R.id.artwork, bm);
            } else {
                builder.setViewVisibility(R.id.artwork, View.GONE);
            }
        }

        return builder.build();
    }

    @Override
    public void updateIcon() {
        updateIcon(createIntent(getString(R.string.tap_restart), "", false));
    }

    void headset(boolean b, final boolean playing) {
        if (b) {
            if (msc == null) {
                Log.d(TAG, "headset mediabutton on");
                msc = new MediaSessionCompat(this, TAG, new ComponentName(this, TorrentReceiver.class), null);
                final TorrentApplication app = TorrentApplication.from(this);
                msc.setCallback(new MediaSessionCompat.Callback() {
                    @Override
                    public void onPlay() {
                        pause();
                    }

                    @Override
                    public void onPause() {
                        pause();
                    }

                    @Override
                    public void onStop() {
                        app.playerStop();
                    }

                    @Override
                    public void onSkipToNext() {
                        if (app.player == null)
                            return;
                        int i = app.player.getPlaying() + 1;
                        if (i >= app.player.getSize())
                            i = 0;
                        app.player.play(i);
                    }

                    @Override
                    public void onSkipToPrevious() {
                        if (app.player == null)
                            return;
                        int i = app.player.getPlaying() - 1;
                        if (i < 0)
                            i = app.player.getSize() - 1;
                        app.player.play(i);
                    }
                });
                msc.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_QUEUE_COMMANDS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
                msc.setActive(true);
                msc.setPlaybackState(new PlaybackStateCompat.Builder().setState(PlaybackStateCompat.STATE_PLAYING, 0, 1).build()); // bug, when after device reboots we have to set playing state to 'playing' to make mediabutton work
            }
            PlaybackStateCompat.Builder builder = new PlaybackStateCompat.Builder()
                    .setActions(PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_PAUSE | PlaybackStateCompat.ACTION_PLAY_PAUSE |
                            PlaybackStateCompat.ACTION_STOP | PlaybackStateCompat.ACTION_SKIP_TO_NEXT | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)
                    .setState(playing ? PlaybackStateCompat.STATE_PLAYING : PlaybackStateCompat.STATE_PAUSED, 0, 1);
            msc.setPlaybackState(builder.build());
        } else {
            if (msc != null) {
                Log.d(TAG, "headset mediabutton off");
                msc.release();
                msc = null;
            }
        }
    }

    void pause() {
        try {
            pause.send();
        } catch (PendingIntent.CanceledException e) {
            Log.d(TAG, "canceled expcetion", e);
        }
    }
}
