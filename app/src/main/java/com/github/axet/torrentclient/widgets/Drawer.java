package com.github.axet.torrentclient.widgets;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.net.HttpClient;
import com.github.axet.androidlibrary.widgets.AboutPreferenceCompat;
import com.github.axet.androidlibrary.widgets.AdminPreferenceCompat;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.OpenChoicer;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.androidlibrary.widgets.RemoteNotificationCompat;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.androidlibrary.widgets.UnreadCountDrawable;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.app.EnginesManager;
import com.github.axet.torrentclient.app.SearchEngine;
import com.github.axet.torrentclient.app.SearchProxy;
import com.github.axet.torrentclient.app.Storage;
import com.github.axet.torrentclient.app.TorrentApplication;
import com.github.axet.torrentclient.navigators.Search;
import com.github.axet.torrentclient.navigators.Torrents;
import com.github.axet.torrentclient.net.GoogleProxy;
import com.github.axet.torrentclient.net.TorProxy;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import libtorrent.Libtorrent;

public class Drawer implements com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener, UnreadCountDrawable.UnreadCount {
    public static final String TAG = Drawer.class.getSimpleName();

    public static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};

    public static String VERSION_CHECK = "https://gitlab.com/axet/android-torrent-client/tags";
    public static int[] DEVELOPERS = new int[]{0xc38af5bf, 0x3feda1d1}; // 0xc38af5bf release, 0x3feda1d1 debug

    static final long INFO_MANUAL_REFRESH = 5 * AlarmManager.SEC1; // prevent refresh if button hit often then 5 seconds
    static final long INFO_AUTO_REFRESH = 5 * AlarmManager.MIN1; // ping external port on drawer open not often then 5 minutes
    static final long ENGINES_AUTO_REFRESH = 12 * AlarmManager.HOUR1; // auto refresh engines every 12 hours

    Context context;
    MainActivity main;
    Handler handler = new Handler();
    OpenChoicer choicer;

    View navigationHeader;
    com.mikepenz.materialdrawer.Drawer drawer;
    UnreadCountDrawable unread;

    Thread update;
    Thread updateOne;
    int updateOneIndex;

    Thread infoThread;
    List<String> infoOld;
    Boolean infoPort;
    long infoTime; // last time checked

    HashMap<ProxyDrawerItem, ProxyDrawerItem.ViewHolder> viewList = new HashMap<>();

    ItemTouchHelper touchHelper;

    long counter = 1;

    public static boolean signatureCheck(Context context, int[] dd) {
        for (int d : dd) {
            if (signatureCheck(context, d))
                return true;
        }
        return false;
    }

    public static boolean signatureCheck(Context context, int d) {
        try {
            PackageManager pm = context.getPackageManager();
            Signature[] ss = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            for (Signature s : ss) {
                int hash = s.hashCode();
                if (d == hash)
                    return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    public Drawer(final MainActivity main, final Toolbar toolbar) {
        this.context = main;
        this.main = main;

        LayoutInflater inflater = LayoutInflater.from(context);

        navigationHeader = inflater.inflate(R.layout.nav_header_main, null, false);

        drawer = new DrawerBuilder()
                .withActivity(main)
                .withToolbar(toolbar)
                .withHeader(navigationHeader)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withOnDrawerListener(new com.mikepenz.materialdrawer.Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        View view = main.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        long time = System.currentTimeMillis();
                        EnginesManager engies = main.getEngines();
                        long t = engies.getTime();
                        if (t + ENGINES_AUTO_REFRESH < time) {
                            refreshEngines(true);
                        }
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }
                })
                .withOnDrawerItemClickListener(this)
                .build();

        touchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                if (!(viewHolder instanceof SearchDrawerItem.ViewHolder))
                    return false;
                if (!(target instanceof SearchDrawerItem.ViewHolder))
                    return false;

                ItemAdapter<IDrawerItem> ad = drawer.getItemAdapter();
                int i = ad.getFastAdapter().getHolderAdapterPosition(viewHolder);
                int k = ad.getFastAdapter().getHolderAdapterPosition(target);
                ad.move(i, k);

                final EnginesManager engines = main.getEngines();
                engines.move(engines.indexOf(ad.getItem(i).getTag()), engines.indexOf(ad.getItem(k).getTag()));

                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            }

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (!(viewHolder instanceof SearchDrawerItem.ViewHolder))
                    return makeMovementFlags(0, 0);
                return super.getMovementFlags(recyclerView, viewHolder);
            }
        });
        touchHelper.attachToRecyclerView(drawer.getRecyclerView());

        Drawable navigationIcon = toolbar.getNavigationIcon();
        unread = new UnreadCountDrawable(context, navigationIcon, Drawer.this);
        unread.setPadding(ThemeUtils.dp2px(main, 15));
        toolbar.setNavigationIcon(unread);

        TextView ver = (TextView) navigationHeader.findViewById(R.id.nav_version);
        AboutPreferenceCompat.setVersion(ver);
    }

    public void close() {
    }

    public void setCheckedItem(long id) {
        drawer.setSelection(id, false);
    }

    public boolean isDrawerOpen() {
        return drawer.isDrawerOpen();
    }

    public void openDrawer() {
        drawer.openDrawer();
    }

    public void closeDrawer() {
        drawer.closeDrawer();
    }

    public void updateUnread() {
        unread.update();
    }

    long getEngineId(Search s) {
        ArrayList<IDrawerItem> list = new ArrayList<IDrawerItem>(drawer.getDrawerItems());
        for (int i = 0; i < list.size(); i++) {
            IDrawerItem item = list.get(i);
            Search search = (Search) item.getTag();
            if (search == s) {
                return item.getIdentifier();
            }
        }
        return -1;
    }

    long getEngineId(List<IDrawerItem> ll, Search s) {
        ArrayList<IDrawerItem> list = new ArrayList<>(drawer.getDrawerItems());
        long id = getEngineId(s);
        if (id != -1)
            return id;
        list.addAll(ll);
        for (int i = 0; i < list.size(); i++) {
            IDrawerItem item = list.get(i);
            if (item.getIdentifier() == counter) {
                counter++;
                if (counter >= 0x00ffffff) // save to set < 0x00ffffff. check View.generateViewId()
                    counter = 1;
                i = -1; // restart search
            }
        }
        return counter;
    }

    public void updateManager() {
        List<IDrawerItem> list = new ArrayList<>();

        final Torrents torrents = main.getTorrents();
        if (torrents == null) {
            ProgressDrawerItem progress = new ProgressDrawerItem();
            progress.withIdentifier(R.id.progress);
            progress.withSelectable(true);
            progress.withSetSelected(false);
            list.add(progress);
            update(list);
            return;
        }

        PrimaryDrawerItem tt = new PrimaryDrawerItem();
        tt.withIdentifier(R.id.nav_torrents);
        tt.withName(R.string.torrents);
        tt.withIcon(new UnreadCountDrawable(context, R.drawable.ic_storage_black_24dp, torrents));
        tt.withIconTintingEnabled(true);
        tt.withSelectable(true);
        tt.withSetSelected(main.active(torrents));
        list.add(tt);

        KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        final boolean locked = myKM.inKeyguardRestrictedInputMode();

        final EnginesManager engines = main.getEngines();

        for (int i = 0; i < engines.getCount(); i++) {
            final Search search = engines.get(i);
            final SearchEngine engine = search.getEngine();

            final int fi = i;
            SearchDrawerItem item = new SearchDrawerItem() {
                @Override
                public void bindView(ViewHolder viewHolder) {
                    super.bindView(viewHolder);

                    viewHolder.release.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (updateOne != null)
                                updateOne.interrupt();
                            updateOneIndex = fi;
                            updateOne = new Thread("Update Engine") {
                                @Override
                                public void run() {
                                    engines.update(fi);
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            updateOne = null;
                                            Search search = engines.get(fi);
                                            main.show(search); // reopen search engine
                                            SearchEngine engine = search.getEngine();
                                            Toast.makeText(context, engine.getName() + context.getString(R.string.engine_updated) + engine.getVersion(), Toast.LENGTH_SHORT).show();
                                            updateManager(); // update proxies list
                                        }
                                    });
                                }
                            };
                            updateOne.start();
                        }
                    });

                    viewHolder.panel.setVisibility(View.GONE);
                    if (updateOne != null && updateOneIndex == fi) {
                        viewHolder.progress.setVisibility(View.VISIBLE);
                        viewHolder.panel.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.progress.setVisibility(View.INVISIBLE);
                    }

                    if (engines.getUpdate(fi)) {
                        viewHolder.release.setVisibility(View.VISIBLE);
                        viewHolder.panel.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.release.setVisibility(View.INVISIBLE);
                    }

                    viewHolder.trash.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(context.getString(R.string.delete_search));

                            String name = engine.getName();

                            builder.setMessage(name + "\n\n" + context.getString(R.string.are_you_sure));
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();

                                    main.openTorrents();
                                    engines.remove(search);

                                    engines.save();
                                    updateManager();
                                }
                            });
                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        }
                    });
                    if (locked) {
                        viewHolder.trash.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        });
                        viewHolder.trash.setColorFilter(Color.GRAY);
                    } else {
                        viewHolder.trash.setColorFilter(ThemeUtils.getThemeColor(context, R.attr.colorAccent));
                    }
                }
            };
            item.withIdentifier(getEngineId(list, search));
            item.withTag(search);
            item.withName(engine.getName());
            item.withIconTintingEnabled(true);
            item.withIcon(new UnreadCountDrawable(context, R.drawable.ic_drag_handle_black_24dp, search));
            item.withSelectable(true);
            item.withSetSelected(main.active(search));
            list.add(item);
        }

        updateProxies(list);

        list.add(new SectionDrawerItem()
                .withIdentifier(R.string.menu_settings)
                .withName(R.string.menu_settings));

        AddDrawerItem navadd = new AddDrawerItem() {
            @Override
            public void bindView(ViewHolder viewHolder) {
                super.bindView(viewHolder);
                viewHolder.update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Drawer.this.update != null) {
                            Drawer.this.update.interrupt();
                            Drawer.this.update = null;
                            updateManager();
                        } else {
                            refreshEngines(false);
                        }
                    }
                });
                if (Drawer.this.update != null) {
                    viewHolder.progress.setVisibility(View.VISIBLE);
                    viewHolder.refresh.setVisibility(View.INVISIBLE);
                } else {
                    viewHolder.progress.setVisibility(View.INVISIBLE);
                    viewHolder.refresh.setVisibility(View.VISIBLE);
                }
                if (locked) {
                    viewHolder.update.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                    viewHolder.refresh.setColorFilter(Color.GRAY);
                    viewHolder.update.setEnabled(false);
                } else {
                    viewHolder.refresh.setColorFilter(ThemeUtils.getThemeColor(context, R.attr.colorAccent));
                    viewHolder.update.setEnabled(true);
                }
                withEnabled(!locked);
            }
        };
        navadd.withIdentifier(R.id.nav_add)
                .withName(R.string.add_search_engine)
                .withIcon(R.drawable.ic_add_black_24dp)
                .withIconTintingEnabled(true)
                .withSelectable(false);
        list.add(navadd);

        SecondaryDrawerItem desc = new SecondaryDrawerItem() {
            @Override
            public void bindView(ViewHolder viewHolder) {
                super.bindView(viewHolder);
                View name = viewHolder.itemView.findViewById(R.id.material_drawer_name);
                name.setVisibility(View.GONE);
                TextView t = (TextView) viewHolder.itemView.findViewById(R.id.material_drawer_description);
                t.setLines(-1);
                t.setSingleLine(false);
            }
        };
        desc.withDescription(R.string.more_engines);
        desc.withIconTintingEnabled(false);
        desc.withSelectable(false);
        list.add(desc);

        update(list);
    }

    void update(List<IDrawerItem> list) {
        ItemAdapter<IDrawerItem> ad = drawer.getItemAdapter();
        for (int i = ad.getAdapterItemCount() - 1; i >= 0; i--) {
            boolean delete = true;
            for (int k = 0; k < list.size(); k++) {
                if (list.get(k).getIdentifier() == ad.getAdapterItem(i).getIdentifier()) {
                    delete = false;
                    ad.set(ad.getGlobalPosition(i), list.get(k));
                }
            }
            if (delete) {
                ad.remove(ad.getGlobalPosition(i));
            }
        }
        for (int k = 0; k < list.size(); k++) {
            boolean add = true;
            for (int i = 0; i < ad.getAdapterItemCount(); i++) {
                if (list.get(k).getIdentifier() == ad.getAdapterItem(i).getIdentifier()) {
                    add = false;
                }
            }
            if (add) {
                ad.add(ad.getGlobalPosition(k), list.get(k));
            }
        }
    }

    public void refreshEngines(final boolean auto) {
        if (auto && main.getTorrents() == null)
            return;
        if (update != null) {
            if (auto)
                return;
        }
        final EnginesManager engines = main.getEngines();
        final Thread old = update;
        update = new Thread("Engines Update") {
            @Override
            public void run() {
                if (old != null) {
                    old.interrupt();
                    try {
                        old.join();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        return;
                    }
                }
                boolean a = auto;

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        updateManager();
                    }
                });
                try {
                    versionCheck();
                    engines.refresh();
                } catch (final RuntimeException e) {
                    Log.e(TAG, "Update Engine", e);
                    // only report errors for current active update thread
                    if (update == Thread.currentThread()) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, ErrorDialog.toMessage(e), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    a = true; // hide update toast on error
                }
                final boolean b = a;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        update = null;
                        if (!b) {
                            if (!engines.updates())
                                Toast.makeText(context, R.string.no_updates, Toast.LENGTH_SHORT).show();
                        }
                        updateManager();
                    }
                });
            }
        };
        update.start();
    }

    void versionCheck() {
        AdminPreferenceCompat.Installer installer = AdminPreferenceCompat.getInstaller(context);

        if (installer == AdminPreferenceCompat.Installer.STORE) // no version check for play store
            return;

        if (installer == AdminPreferenceCompat.Installer.APK && !signatureCheck(context, DEVELOPERS)) // no version check for releases signed by other side
            return;

        final View b = navigationHeader.findViewById(R.id.search_engine_new);
        String url = VERSION_CHECK;
        if (url != null && !url.isEmpty()) {
            HttpClient client = new HttpClient();
            HttpClient.DownloadResponse w = client.getResponse(null, url);
            w.download();
            if (w.getError() == null) { // throw new RuntimeException(w.getError() + ": " + url);
                String html = w.getHtml();
                try {
                    PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    String v = pInfo.versionName;
                    final String version = Search.matcher(html, ".ref-name:regex(.*-(.*))", "");
                    if (!v.equals(version)) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                b.setVisibility(View.VISIBLE);
                                b.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Toast.makeText(context, context.getString(R.string.new_version) + " v" + version, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        });
                        return;
                    }
                } catch (PackageManager.NameNotFoundException ignore) {
                }
            }
        }
        b.setVisibility(View.GONE);
    }

    void updateProxies(List<IDrawerItem> list) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View up = inflater.inflate(R.layout.drawer_search_plus, null);
        list.add(new SectionPlusDrawerItem(up)
                .withIdentifier(R.string.web_proxy_s)
                .withName(R.string.web_proxy_s));

        viewList.clear();

        ProxyDrawerItem google = createProxy(GoogleProxy.NAME, R.string.google_proxy);
        list.add(google);

        if (TorProxy.isOrbotInstalled(context)) {
            ProxyDrawerItem tor = createProxy(TorProxy.NAME, R.string.tor_proxy);
            list.add(tor);
        } else { // clear TOR proxy
            final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
            String tag = shared.getString(TorrentApplication.PREFERENCE_PROXY, "");
            if (tag.equals(TorProxy.NAME)) {
                SharedPreferences.Editor edit = shared.edit();
                edit.putString(TorrentApplication.PREFERENCE_PROXY, "");
                edit.commit();
            }
        }

        RecyclerView.Adapter a = main.getAdapter();
        if (a instanceof Search) {
            final SearchEngine engine = ((Search) a).getEngine();
            final SearchProxy p = engine.getProxy();
            if (p != null) {
                final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
                ProxyDrawerItem custom = new ProxyDrawerItem() {
                    @Override
                    public void bindView(final ViewHolder viewHolder) {
                        super.bindView(viewHolder);
                        viewHolder.tag = p.name;
                        viewList.put(this, viewHolder);
                        final Runnable update = new Runnable() {
                            @Override
                            public void run() {
                                String proxy = shared.getString(TorrentApplication.PREFERENCE_PROXY, "");
                                String proxyp = shared.getString(TorrentApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), "");
                                if (proxyp.equals(p.name)) {
                                    viewHolder.w.setChecked(true);
                                    for (ViewHolder h : viewList.values()) {
                                        if (h == viewHolder)
                                            continue;
                                        h.w.setChecked(false);
                                    }
                                } else {
                                    for (ProxyDrawerItem i : viewList.keySet()) {
                                        ProxyDrawerItem.ViewHolder h = viewList.get(i);
                                        h.w.setChecked(h.tag.equals(proxy));
                                    }
                                }
                            }
                        };
                        viewHolder.w.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences.Editor edit = shared.edit();
                                if (viewHolder.w.isChecked()) {
                                    edit.putString(TorrentApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), p.name);
                                } else {
                                    edit.putString(TorrentApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), "");
                                }
                                edit.commit();
                                update.run();
                            }
                        });
                        update.run();
                    }
                };
                custom.withName(p.name);
                custom.withIcon(R.drawable.ic_vpn_key_black_24dp);
                custom.withIconTintingEnabled(true);
                custom.withSelectable(false);
                list.add(custom);
            }
        }
    }

    ProxyDrawerItem createProxy(final String tag, final int res) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        final String proxy = shared.getString(TorrentApplication.PREFERENCE_PROXY, "");
        ProxyDrawerItem google = new ProxyDrawerItem() {
            @Override
            public void bindView(final ViewHolder viewHolder) {
                super.bindView(viewHolder);
                viewHolder.tag = tag;
                viewList.put(this, viewHolder);
                viewHolder.w.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences.Editor edit = shared.edit();
                        if (viewHolder.w.isChecked()) {
                            edit.putString(TorrentApplication.PREFERENCE_PROXY, tag);
                            RecyclerView.Adapter a = main.getAdapter();
                            if (a instanceof Search) { // remove custom proxy
                                final SearchEngine engine = ((Search) a).getEngine();
                                String proxyp = shared.getString(TorrentApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), "");
                                if (!proxyp.isEmpty())
                                    edit.putString(TorrentApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), "");
                            }
                        } else {
                            edit.putString(TorrentApplication.PREFERENCE_PROXY, "");
                        }
                        edit.commit();
                        for (ViewHolder h : viewList.values()) {
                            if (h == viewHolder)
                                continue;
                            h.w.setChecked(false);
                        }
                    }
                });
                viewHolder.w.setChecked(proxy.equals(tag));
            }
        };
        google.withIdentifier(res);
        google.withName(res);
        google.withIcon(R.drawable.ic_vpn_key_black_24dp);
        google.withIconTintingEnabled(true);
        google.withSelectable(false);
        return google;
    }

    public void openDrawer(Search search) {
        openDrawer();
        EnginesManager engies = main.getEngines();
        for (int i = 0; i < engies.getCount(); i++) {
            if (engies.get(i) == search) {
                setCheckedItem(getEngineId(search));
                main.show(search);
                main.getDrawer().updateManager(); // update proxies
                return;
            }
        }
    }

    public void updateHeader() {
        ArrayList<String> info = new ArrayList<>();
        long c = Libtorrent.portCount();
        for (long i = 0; i < c; i++)
            info.add(Libtorrent.port(i));

        StringBuilder str = new StringBuilder();
        for (String ip : info) {
            str.append(ip);
            str.append("\n");
        }
        final String ss = str.toString().trim();

        TextView textView = (TextView) navigationHeader.findViewById(R.id.torrent_ip);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, ss, Toast.LENGTH_SHORT).show();
            }
        });
        textView.setText(ss);

        View portButton = navigationHeader.findViewById(R.id.torrent_port_button);
        ImageView portIcon = (ImageView) navigationHeader.findViewById(R.id.torrent_port_icon);
        TextView port = (TextView) navigationHeader.findViewById(R.id.torrent_port_text);

        portButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long time = System.currentTimeMillis();
                if (infoTime + INFO_MANUAL_REFRESH < time) {
                    infoTime = time;
                    infoOld = null;
                }
            }
        });

        long time = System.currentTimeMillis();
        if (infoTime + INFO_AUTO_REFRESH < time) {
            infoTime = time;
            infoOld = null;
        }

        if (infoOld == null || !Arrays.equals(info.toArray(), infoOld.toArray())) {
            if (drawer.isDrawerOpen()) { // only probe port when drawer is open
                if (infoThread != null)
                    return;
                infoOld = info;
                infoThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final boolean b = Libtorrent.portCheck();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    infoPort = b;
                                    infoThread = null;
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    infoPort = null;
                                    infoThread = null;
                                }
                            });
                        }
                    }
                }, "Port Check");
                infoThread.start();
                infoPort = false;
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_checking);
            } else {
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_closed);
            }
        } else {
            if (infoPort == null) {
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_down);
            } else if (infoPort) {
                portIcon.setImageResource(R.drawable.port_ok);
                port.setText(R.string.port_open);
            } else {
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_closed);
            }
        }
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        long id = drawerItem.getIdentifier();

        final EnginesManager engies = main.getEngines();

        // here only two types of adapters, so setup empty view manually here.

        if (id == R.id.nav_torrents) {
            main.openTorrents();
            closeDrawer();
            return true;
        }

        if (drawerItem.getTag() != null) {
            Search search = (Search) drawerItem.getTag();
            main.show(search);
            updateManager(); // update proxy list
            engies.save();
            closeDrawer();
            return true;
        }

        if (id == R.id.nav_add) {
            Uri u = TorrentApplication.from(context).storage.getStoragePath();
            String path = TorrentApplication.getPreferenceLastPath(context);
            if (path.startsWith(ContentResolver.SCHEME_FILE))
                u = Uri.fromFile(new File(path));
            choicer = new OpenChoicer(OpenFileDialog.DIALOG_TYPE.FILE_DIALOG, true) {
                @Override
                public void onResult(Uri uri) {
                    String s = uri.getScheme();
                    if (s.equals(ContentResolver.SCHEME_FILE)) {
                        File f = Storage.getFile(uri);
                        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
                        shared.edit().putString(TorrentApplication.PREFERENCE_LAST_PATH, f.getParent()).commit();
                    }
                    save(uri);
                }
            };
            choicer.setPermissionsDialog(main, PERMISSIONS, MainActivity.RESULT_ADD_ENGINE);
            choicer.setStorageAccessFramework(main, MainActivity.RESULT_ADD_ENGINE);
            choicer.show(u);
            return true; // prevent close drawer
        }
        return true;
    }

    @Override
    public int getUnreadCount() {
        int count = 0;
        Torrents torrents = main.getTorrents();
        if (torrents != null)
            count += torrents.getUnreadCount();
        EnginesManager engies = main.getEngines();
        if (engies != null) {
            for (int i = 0; i < engies.getCount(); i++) {
                count += engies.get(i).getUnreadCount();
            }
        }
        return count;
    }

    @TargetApi(21)
    public void onActivityResult(int resultCode, Intent data) {
        choicer.onActivityResult(resultCode, data);
    }

    public void onRequestPermissionsResult(String[] permissions, int[] grantResults) {
        choicer.onRequestPermissionsResult(permissions, grantResults);
    }

    void save(Uri p) {
        final EnginesManager engies = main.getEngines();
        Search search;
        try {
            search = engies.add(p);
        } catch (RuntimeException e) {
            ErrorDialog.Error(main, e);
            return;
        }
        engies.save();
        updateManager();
        openDrawer(search);
    }
}
